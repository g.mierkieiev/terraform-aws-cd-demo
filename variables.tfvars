region = "us-east-1"

vpc_cidr               = "10.0.0.0/16"
private_subnet_cidr_1a = "10.0.1.0/24"
private_subnet_cidr_1b = "10.0.2.0/24"
private_subnet_cidr_1c = "10.0.3.0/24"
public_subnet_cidr_1a  = "10.0.4.0/24"
public_subnet_cidr_1b  = "10.0.5.0/24"
public_subnet_cidr_1c  = "10.0.6.0/24"

tags = {
  Name        = "demo"
  Owner       = "sysops"
  ManagedBy   = "terraform"
  Environment = "demo"
}