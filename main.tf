terraform {
  backend "s3" {}
}

module "vpc" {
  source = "./modules/terraform-aws-vpc"

  name = var.tags.Name
  cidr = var.vpc_cidr

  azs             = ["${var.region}a", "${var.region}b", "${var.region}c"]
  private_subnets = [var.private_subnet_cidr_1a, var.private_subnet_cidr_1b, var.private_subnet_cidr_1c]
  public_subnets  = [var.public_subnet_cidr_1a, var.public_subnet_cidr_1b, var.public_subnet_cidr_1c]

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = var.tags
}