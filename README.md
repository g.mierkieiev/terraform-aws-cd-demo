# Gitlab CI pipeline

Gitlab CI pipeline which creates resources on the AWS

### Before you begin

#### Create s3

```bash
aws s3api create-bucket --bucket terraform-backend-store \
    --region eu-west-1 \
    --create-bucket-configuration \
    LocationConstraint=eu-west-1
```

#### Enable bucket encryption

```bash
aws s3api put-bucket-encryption \
    --bucket terraform-backend-store \
    --server-side-encryption-configuration={\"Rules\":[{\"ApplyServerSideEncryptionByDefault\":{\"SSEAlgorithm\":\"AES256\"}}]}
```

#### Create IAM user

```bash
aws iam create-user --user-name terraform-deployer
```

#### Create IAM group

```bash
aws iam create-group --group-name terraform-deployer-group
```


#### Add IAM user to group

```bash
aws iam add-user-to-group --user-name terraform-deployer --group-name terraform-deployer-group
```

#### Activate IAM programmatic access

```bash
aws iam create-access-key --user-name terraform-deployer

Output:
{
    "AccessKey": {
        "UserName": "terraform-deployer",
        "Status": "Active",
        "CreateDate": "2015-03-09T18:39:23.411Z",
        "SecretAccessKey": "wJalrXUtnFEMI/K7MDENG/bPxRfiCYzEXAMPLEKEY",
        "AccessKeyId": "AKIAIOSFODNN7EXAMPLE"
    }
}
```

Save `SecretAccessKey` and `AccessKeyId`

# Usage

The first thing you should to do is create variables with programmatic access keys in the gitlab vars
Go to `Settings` -> `CI/CD`. Expand `Variables` and create new variables

`DEMO_AWS_ACCESS_KEY_ID` - for AccessKeyId (see output `Activate IAM programmatic access`)
`DEMO_AWS_SECRET_ACCESS_KEY` - for SecretAccessKey (see output `Activate IAM programmatic access`)
`DEMO_AWS_DEFAULT_REGION` - your default region (in code you can change region if some of resource should to be run in different region)

After creating variables you can push your code into repo and deploy desired resources.

## Gitlab pipeline

There are 4 stages in the pipeline

```yaml
stages:
  - validate
  - plan
  - apply
  - destroy
```

### validate

This stage starts always in any branches

### plan

Starts only on `merge_requests`. This stage is important, it executes the `terraform plan` and saves it as an artifact for subsequent stages.
After merge `plan` will be executed again, but using the artifact in the previous `plan`.

### apply

Only starts in desired branch. For example, if you would like to apply resources in the main branch.
In this example in this example it is used as the main branch of the `demo`, you easily change branch in the pipeline in the stage `apply`, example:

```yaml
  only:
    - demo
```

This stage executes manually, this means that to create resources, you need the approval of the release manager, and only after he pushes on the start button, the resources stage will start.

### destroy

This stage is only available after a successful `apply` stage and starts only on `main` branch.
Release manager can manually destroy all resources.

## Step by step introduction

 - Create new branch to wor with
 - Push code into repository
 - After pushing, go to gitlab repo via web `CI/CD` -> `Pipelines` and you will see following picture. 
![alt text](media/stage-validate.png "Validate stage")
   If you see status `passed` you can continue to the next steps
 - after successful validation you can create merge request and can check how plan is going
![alt text](media/stage-plan-before-merge.png "Plan stage before merge")
   After successful `plan` you can merge it
 - When the branch is merged into the master, the validation and plan stages will run again, and then the release manager must press the play button at the apply stage
![alt text](media/stage-val-plan-apply-destroy.png "Stages after merge")
 - Same way for `destroy`. Release manager can play the `destroy` stage, which will start the process of deleting all the resources that were created by the terraform.

In total at the end everything should look like this in the pipeline:
![alt text](media/stage-all-summary.png "Summary")